<?php
/*
 * @file uc_labels.theme.inc
 * Provides theming of the order overview table
 * @copyright Copyright(c) 2011 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Default implementation of theme_uc_labels_order_admin
 * @param $form array
*/
function theme_uc_labels_order_admin($form) {
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Actions')),
    array('data' => t('Order ID'), 'field' => 'o.order_id', 'sort' => 'desc'),
    array('data' => t('Customer')),
    array('data' => t('Total'), 'align' => 'center', 'field' => 'o.order_total'),
    array('data' => t('Purchase date'), 'align' => 'center', 'field' => 'o.created'),
    array('data' => t('Status'), 'field' => 'os.title'),
  );
  
  drupal_add_js(array(
    'ucURL' => array(
      'adminOrders' => url('admin/store/orders/'),
    ),
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'uc_order') .'/uc_order.js');
  
  $rows = array();
  foreach ($form['#rows'] as $order_id => $row) {
    array_unshift($row['data'], drupal_render($form['orders'][$order_id]));
    $rows[] = $row;
  }
  
  $output = theme('table', $header, $rows, array('class' => 'uc-orders-table'));
  $output .= theme('pager', NULL, variable_get('uc_order_number_displayed', 30), 0);
  $output .= drupal_render($form);
  return $output;
}
