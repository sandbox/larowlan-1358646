<?php
/*
 * @file uc_labels.labels.inc
 * Provides label processing logic
 * @copyright Copyright(c) 2011 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Generate an imagick object for a page
 * @param $width int the page width in mm
 * @param $height int the page height in mm
 * @param $resolution int the resolution
 * @param $format string (pdf/jpg)
 * 
*/
function uc_labels_generate_page($width, $height, $resolution, $format = 'png') {
  $image = new Imagick();
  $image->newImage(
    uc_labels_mm_to_pixels($width, $resolution),
    uc_labels_mm_to_pixels($height, $resolution),
    new ImagickPixel('#ffffff')); //new image w/ transparency
  $image->setResolution($resolution, $resolution);
  
  //set format
  $image->setImageFormat($format);
  $image->setImageColorspace(imagick::COLORSPACE_RGB);
  $image->setCompression(imagick::COMPRESSION_ZIP);
  $image->setCompressionQuality(100);
  return $image;
}


/**
 * Converts mm to pixels
 * @param $mm int millimeters
 * @param $dpi int dots per inch
 * @return int pixels
*/
function uc_labels_mm_to_pixels($mm, $dpi) {
  $inches = $mm / 25.4; //25.4 mm per inch
  return $inches * $dpi;
}

/**
 * Util to render text on a label
 * @param $image the image where the text will be rendered
 * @param $text string the text to render
 * @param $width float the width of the tile in px
 * @param $height float the $height of the tile in px
 * @param $resoultion int the resolution in dpi
 * @param $fontname string the fontname to use 
 * @param $color string hex color of text
 * @param $fontsize int fontsize in pts
 * @param $bgcolor string hex color of background
 * @param $align string left/right/center
 * @param $rotate bool rotate the text
 * @return object Imagick object containing the text
*/
function uc_labels_render_text($image, $text, $width, $height, $resolution, $fontname = 'arial',
                                  $color = '000000', $bgcolor = FALSE,
                                  $align = 'left') {  
  //setup background color
  $background = new ImagickPixel('#ffffff'); // Transparent
  if ($bgcolor) {
    $background = new ImagickPixel('#'. $bgcolor); //there is a colour
  }
  
  $tile_img = new Imagick();
  $tile_img->newImage($width,
                    $height,
                    $background);
  $tile_img->setResolution($resolution, $resolution);
  $tile_img->setImageFormat('png');
  
  $fontpath = drupal_get_path('module', 'uc_labels') .'/fonts/';

  $size = variable_get('uc_labels_fontsize', 12);
  
  if ($text && $text != '&nbsp;') { //there is text (not just a rectangle)
    //swap br for nl
    $text = str_replace(array('<br>', '<br />', '<BR />', '<BR>'), "\n", $text);
    //setup our drawing item
    $draw = new ImagickDraw();
    //setup color
    $color = new ImagickPixel('#'. str_replace('#', '', $color)); //just to be safe remove the #

    //font file        
    $fontfile = $fontpath . $font . '.ttf';
    if (!is_file($fontfile)) {
      //use default font
      $fontfile = $fontpath .'arial.ttf'; //NB: ok to use arial as not being distributed
    }
    
    //font properties
    $draw->setFont($fontfile);
    $draw->setTextEncoding("UTF-8");
    //1 is LEFT, 2 is CENTER, 3 is RIGHT
    $alignment = array('center' => 2, 'left' => 1, 'right' => 3);
    $draw->setTextAlignment($alignment[$align] ? $alignment[$align]: 1);
    $start_point = 0;
    if ($align == 'center') {
      $start_point = $width/2;
    }
    if ($align == 'right') {
      $start_point = $width;
    }
    $draw->setFontSize((double)$size);
    $draw->setFillColor($color);
    $draw->setStrokeAntialias(true);
    $draw->setTextAntialias(true);
    
    //get font metrics
    $metrics = $image->queryFontMetrics($draw, $text);
    
    //wrap long text
    $wrap = floor($width / $metrics['characterWidth']) * 2; //start conservative
    //max loops
    $loops = 0;
    $original_text = $text;
    while ($metrics['textWidth'] > ($width) && $loops <= 10) { //10 is a buffer
      //we need to wrap
      $text = wordwrap($original_text, $wrap);
      $wrap--;
      $metrics = $tile_img->queryFontMetrics($draw, $text);
      $loops++;
    }
    //do a last minute check of the font size
    if ($fit) {
      $loops = 0;
      $new_size = $size;
      while ($metrics['textHeight'] > ($height) && $loops <= 10) { //10 is a buffer
        //we need to shrink font size
        $new_size *= 0.975; //Shrink by 2.5%
        $draw->setFontSize((double)$new_size);
        $metrics = $image->queryFontMetrics($draw, $text);
        $loops++;
      }
    }
    $draw->annotation($start_point, $metrics['ascender'], $text);
  } //end text processing (otherwise just a rectangle)
  if ($draw) { //there is text (not just a rectangle)
    $tile_img->drawImage($draw);
    return $tile_img;
  };
  return FALSE;
}

/**
 * Split the orders into sets
 * @param $orders array of uc_order objects
 * @param $per_page int number of labels per page
*/
function uc_labels_compile_sets($orders = array(), $per_page = 1) {
  $sets = array();
  $c = 0;
  $this_set = array();
  foreach ($orders as $order) {
    // Note we have to decode as uc_order_address uses check_plain and that's no good for us
    $this_set[$order->order_id] = htmlspecialchars_decode(uc_order_address($order, 'delivery'), ENT_QUOTES); //add this to the page
    $c++;
    // We have enough for a page
    if ($c == $per_page) {
      // add it to the page sets
      $sets[] = $this_set;
      // reset out counter
      $c = 0;
      // reset our page
      $this_set = array();
    }
  }
  // Finally we add our last page (may not be full)
  if (count($this_set)) {
    $sets[] = $this_set;
  }
  return $sets;
}

/**
 * Generate a random temp file
 * @param $seed string random seed
*/
function uc_labels_temp_file($seed) {
  return file_create_filename(md5(rand(0, time()). $seed), file_directory_temp());
}


/**
 * Converts pts to pixels
 * @param $pts int points
 * @param $dpi int dots per inch
 * @return int pixels
*/
function uc_labels_pts_to_pixels($pts, $dpi) {
  $inches = $pts / 72; //72 pts per inch
  return $inches * $dpi;
}
