<?php
/*
 * @file uc_labels.admin.inc
 * Provides the main admin form and order list callback for the module
 * @copyright Copyright(c) 2011 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */

/**
 * Admin form for setting up the layout options
*/
function uc_labels_admin_form($form_state) {
  $form = array();
  $form['uc_labels_resolution'] = array(
    '#type' => 'select',
    '#title' => t('PDF resolution'),
    '#default_value' => variable_get('uc_labels_resolution', 72),
    '#options' => array(
      72 => t('72 dpi'),
      150 => t('150 dpi'),
      300 => t('300 dpi'),
    ),
    '#description' => t('Chose the pdf resolution required for the labels'),
  );
  
  $form['page'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => FALSE,
    '#description'  => t('Setup your page dimensions'),
    '#title'        => t('Page dimensions'),
  );
  
  $form['page']['uc_labels_page_height'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page height'),
    '#default_value' => variable_get('uc_labels_page_height', 297),
    '#size'          => 20,
    '#field_suffix'  => t('mm'),
    '#description'   => t('Enter the page height in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['page']['uc_labels_page_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page width'),
    '#default_value' => variable_get('uc_labels_page_width', 210),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the page width in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['page']['margins'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => FALSE,
    '#description'  => t('Setup your page margins'),
    '#title'        => t('Margins'),
  );
  
  $form['page']['margins']['uc_labels_margin_top'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page margin (top)'),
    '#default_value' => variable_get('uc_labels_margin_top', 23),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the top margin in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['page']['margins']['uc_labels_margin_bottom'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page margin (bottom)'),
    '#default_value' => variable_get('uc_labels_margin_bottom', 24),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the bottom margin in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['page']['margins']['uc_labels_margin_left'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page margin (left)'),
    '#default_value' => variable_get('uc_labels_margin_left', 0),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the left margin in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['page']['margins']['uc_labels_margin_right'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Page margin (right)'),
    '#default_value' => variable_get('uc_labels_margin_right', 0),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the right margin in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => FALSE,
    '#description'  => t('Setup your labels dimensions and layout'),
    '#title'        => t('Label dimensions and layout'),
  );
  
  $form['labels']['uc_labels_labels_across'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Labels across'),
    '#default_value' => variable_get('uc_labels_labels_across', 3),
    '#size'          => 20,
    '#description'   => t('Enter the number of labels across'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_labels_down'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Labels down'),
    '#default_value' => variable_get('uc_labels_labels_down', 5),
    '#size'          => 20,
    '#description'   => t('Enter the number of labels down'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_label_width']  = array(
    '#type'          => 'textfield',
    '#title'         => t('Label width'),
    '#default_value' => variable_get('uc_labels_label_width', 70),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the label width in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_label_height']  = array(
    '#type'          => 'textfield',
    '#title'         => t('Label height'),
    '#default_value' => variable_get('uc_labels_label_height', 50),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the label height in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_label_space_across']  = array(
    '#type'          => 'textfield',
    '#title'         => t('Label spacing (across)'),
    '#default_value' => variable_get('uc_labels_label_space_across', 0),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the space between labels across the page in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_label_space_down']  = array(
    '#type'          => 'textfield',
    '#title'         => t('Label spacing (down)'),
    '#default_value' => variable_get('uc_labels_label_space_down', 0),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the space between labels down the page in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_padding']  = array(
    '#type'          => 'textfield',
    '#title'         => t('Label padding (top/left)'),
    '#default_value' => variable_get('uc_labels_padding', 5),
    '#field_suffix'  => t('mm'),
    '#size'          => 20,
    '#description'   => t('Enter the label padding (top/left) in mm'),
    '#maxlength'     => 5,
    '#required'      => TRUE,
  );
  
  $form['labels']['uc_labels_fontsize'] = array(
    '#type'          => 'select',
    '#title'         => t('Font size'),
    '#default_value' => variable_get('uc_labels_fontsize', 12),
    '#description'   => t('Select your fontsize'),
    '#required'      => TRUE,
    '#options'       => drupal_map_assoc(range(10, 16))
  );
  
  return system_settings_form($form);
}

/**
 * Validation handler for the settings form
*/
function uc_labels_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // check is numeric
  $numeric = array('labels_across', 'label_width', 'page_width',
                   'label_space_across', 'labels_down', 'label_height',
                   'page_height', 'label_space_down', 'margin_top',
                   'margin_left', 'margin_right', 'margin_bottom');
  foreach ($numeric as $field) {
    if (!is_numeric($values['uc_labels_'. $field])) {
      form_set_error('uc_labels_'. $field, t('You must enter a numeric value'));
    }
  }
  // These ones have to be integers
  $integers = array('labels_across', 'labels_down');
  foreach ($integers as $field) {
    // We can't use is_integer because form values arrive as strings
    if ((int)$values['uc_labels_'. $field] != (float)$values['uc_labels_'. $field]) {
      form_set_error('uc_labels_'. $field, t('You must enter a whole number'));      
    }
  }
  // Check that (across x label_width) + ((across - 1) x label_space_across) < page_width
  $total_width = $values['uc_labels_labels_across'] * $values['uc_labels_label_width'];
  $total_width += (($values['uc_labels_labels_across'] - 1) * $values['uc_labels_label_space_across']);
  $total_width += ($values['uc_labels_margin_left'] + $values['uc_labels_margin_right']);
  if ($total_width != $values['uc_labels_page_width']) {
    form_set_error('uc_labels_page_width', t('The page width you have entered does not match the label layout (@size) you have specified.', array('@size' => $total_width)));
  }
  // Check that (down x label_height) + ((down - 1) x label_space_down) < page_height
  $total_height = $values['uc_labels_labels_down'] * $values['uc_labels_label_height'];
  $total_height += (($values['uc_labels_labels_down'] - 1) * $values['uc_labels_label_space_down']);
  $total_height += ($values['uc_labels_margin_top'] + $values['uc_labels_margin_bottom']);
  if ($total_height != $values['uc_labels_page_height']) {
    form_set_error('uc_labels_page_height', t('The page height you have entered does not match the label layout (@size) you have specified.', array('@size' => $total_height)));
  }
}

/**
 * Rework the order list at admin/store/orders to include a checkbox for
 * printing the fields - this a form builder callback
*/
function uc_labels_order_admin($form_state, $sql = NULL, $args = NULL, $search = FALSE) {
  module_load_include('inc', 'uc_order', 'uc_order.admin');

  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Actions')),
    array('data' => t('Order ID'), 'field' => 'o.order_id', 'sort' => 'desc'),
    array('data' => t('Customer')),
    array('data' => t('Total'), 'align' => 'center', 'field' => 'o.order_total'),
    array('data' => t('Purchase date'), 'align' => 'center', 'field' => 'o.created'),
    array('data' => t('Status'), 'field' => 'os.title'),
  );

  if (is_null($sql)) {
    $args = array();
    $show_status = 1;
    $sql = 'SELECT o.order_id, o.uid, o.billing_first_name, o.billing_last_name, o.order_total, o.order_status, o.created, os.title '
          .'FROM {uc_orders} o LEFT JOIN {uc_order_statuses} os ON o.order_status = os.order_status_id ';


    if (arg(3) == 'sort' && !is_null(arg(4))) {
      $_SESSION['sort_status'] = arg(4);
      $args[] = arg(4);
    }
    $where = '';
    if (!isset($_SESSION['sort_status']) || $_SESSION['sort_status'] != 'all') {
      if (!empty($_SESSION['sort_status']) && is_string($_SESSION['sort_status'])) {
        $where = "WHERE o.order_status = '%s'";
        $args[] = $_SESSION['sort_status'];
      }
      else {
        $where = 'WHERE o.order_status IN '. uc_order_status_list('general', TRUE);
      }
    }
    $sql .= $where . tablesort_sql($header);
  }

  $address = variable_get('uc_customer_list_address', 'billing');
  if ($address == 'shipping') {
      $sql = str_replace('billing', 'delivery', $sql);
  }
  else {
    $address = 'billing';
  }

  $context = array(
    'revision' => 'themed-original',
    'type' => 'amount',
  );

  $rows = array();

  $order_checkboxes = array();

  $result = pager_query($sql, variable_get('uc_order_number_displayed', 30), 0, NULL, $args);
  while ($order = db_fetch_object($result)) {
    $order_checkboxes[$order->order_id] = '';
    if ($address == 'shipping') {
      $order_name = $order->delivery_first_name .' '. $order->delivery_last_name;
    }
    else {
      $order_name = $order->billing_first_name .' '. $order->billing_last_name;
    }
    if (trim($order_name) == '') {
      if ($order->uid !== 0) {
        $account = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $order->uid));
      }
      if (empty($account)) {
        $order_name = t('User: none');
      }
      else {
        $order_name = t('User: !name', array('!name' => $account));
      }
    }

    $rows[$order->order_id] = array(
      'data' => array(
        array('data' => uc_order_actions($order, TRUE), 'nowrap' => 'nowrap'),
        array('data' => $order->order_id),
        array('data' => check_plain($order_name), 'nowrap' => 'nowrap'),
        array('data' => uc_price($order->order_total, $context), 'align' => 'right', 'nowrap' => 'true'),
        array('data' => format_date($order->created, 'custom', variable_get('uc_date_format_default', 'm/d/Y')), 'align' => 'center'),
        array('data' => $order->title),
      ),
      'id' => 'order-'. $order->order_id,
    );
  }

  $form['orders'] = array(
    '#type' => 'checkboxes',
    '#options' => $order_checkboxes
  );
  $form['#rows'] = $rows;

  $form['labels'] = array(
    '#type' => 'submit',
    '#submit' => array('uc_labels_order_labels_submit'),
    '#value' => t('Print labels for selected')
  );

  if ($search === FALSE) {
    $form['#prefix'] = '<div class="order-overview-form">'. drupal_get_form('uc_order_select_form') .'</div>'
             .'<div class="order-overview-form">'. drupal_get_form('uc_order_admin_sort_form') .'</div>';
  }

  return $form;
}

/**
 * Submission handler for the 'print selected' button
*/
function uc_labels_order_labels_submit($form, &$form_state) {
  $order_ids = array_keys(array_filter($form_state['values']['orders']));
  $orders = array();
  foreach ($order_ids as $order_id) {
    $orders[$order_id] = uc_order_load($order_id);
  }
  return uc_labels_generate_labels($orders);
}


/**
 * Generates the labels
 * @orders array an array of order objects
*/
function uc_labels_generate_labels($orders = array()) {
  //setup some helper vars.
  $variables = array('labels_across' => 3, 'label_width' => 70,
                     'page_width' => 210, 'label_space_across' => 0,
                     'labels_down' => 5, 'label_height' => 50,
                   'page_height' => 297, 'label_space_down' => 0,
                   'resolution' => 72, 'margin_top' => 23,
                   'margin_left' => 0, 'margin_right' => 0,
                   'margin_bottom' => 24, 'padding' => 5);
  foreach ($variables as $variable => $default) {
    $$variable = variable_get('uc_labels_'. $variable, $default);
  }
  $per_page = $labels_across * $labels_down;
  if ($per_page <= 0) {
    drupal_set_message(t('The label configuration is not setup correctly. You cannot have @per_page labels per page.', array('@page' => $per_page)), 'error');
    return;
  }
  module_load_include('inc', 'uc_labels', 'uc_labels.labels');
  $sets = uc_labels_compile_sets($orders, $per_page);
  $pages = array();
  foreach ($sets as $set) {
    $page = uc_labels_generate_page($page_width, $page_height, $resolution);
    $index = 1;
    foreach ($set as $label) {
      $x_offset = uc_labels_mm_to_pixels((($index - 1) % $labels_across) * ($label_width + $label_space_across), $resolution);
      $y_offset = uc_labels_mm_to_pixels(floor(($index - 1) / $labels_across) * ($label_height + $label_space_down), $resolution);
      $tile_img = uc_labels_render_text($page, $label, uc_labels_mm_to_pixels($label_width - 2 * $padding, $resolution),
                                        uc_labels_mm_to_pixels($label_height - 2 * $padding, $resolution),
                                        $resolution);
      //now $tile_img has our label as an image - add it to the page
      $page->compositeImage($tile_img,
        Imagick::COMPOSITE_DEFAULT,
        $x_offset + uc_labels_mm_to_pixels($padding + $margin_left, $resolution),
        $y_offset + uc_labels_mm_to_pixels($padding + $margin_top, $resolution));
      $tile_img->clear();
      $tile_img->destroy();
      $index++;
    }
    $filename = uc_labels_temp_file(implode('-', array_keys($set))) .'.png';
    $page->writeImage($filename);
    $pages[] = $filename;
  }
  if (count($pages)) {
    $file = uc_labels_temp_file(implode('-', array_keys($orders))) .'-full.pdf';
    exec("/usr/local/bin/convert ". implode(' ', $pages) ." $file");
    header("Content-type: application/pdf");
    echo file_get_contents($file);
    exit;
  }
  drupal_set_message(t('No labels found to print'));
  // We have to return something for the single label print menu callback
  return t('No labels found to print');
}

/**
 * Menu callback to generate a single label
 * @param $order object uc_order object
*/
function uc_labels_single_order($order) {
  return uc_labels_generate_labels(array($order->order_id => $order));
}